package Testare;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import Clase.Faina;
import Clase.Lapte;
import Clase.RetetaPrajitura;
import Clase.RetetaTort;
import Clase.Unt;
import Clase.Zahar;

public class Test {

	RetetaPrajitura reteta;
	RetetaTort tort;
	Lapte lapte;
	Faina faina;
	Unt unt;
	Zahar zahar;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		reteta=new RetetaPrajitura();
		lapte=new Lapte();
		faina=new Faina();
		lapte=new Lapte();
		unt=new Unt();
		zahar=new Zahar();
		tort=new RetetaTort();
	}

	@After
	public void tearDown() throws Exception {
	}
	@org.junit.Test
	public void testSumaCantDA() {
		assertEquals(0,reteta.faina.returnCantitate()+reteta.margarina.returnCantitate());
	}
	@org.junit.Test
	public void testSumaCantNU() {
		assertNotEquals(3,reteta.faina.returnCantitate()+reteta.margarina.returnCantitate());
	}
	@org.junit.Test
	public void testProdusPostDA() {
		assertTrue(faina.IngredientDePost());
		assertTrue(zahar.IngredientDePost());
		assertTrue(faina.IngredientDePost());
	}
	@org.junit.Test
	public void testProdusPostNU() {
		assertFalse(lapte.IngredientDePost());
		assertFalse(unt.IngredientDePost());
	}
	@org.junit.Test
	public void testRetetaPostDA() {		
		assertTrue(reteta.retetaPost());		
	}
	@org.junit.Test
	public void testRetetaPostNU() {	
		reteta.lapte.setCantitate(3);
		assertFalse(reteta.retetaPost());		
	}

	@org.junit.Test
	public void testExistaRetetaDA() {
		reteta.margarina.setCantitate(15);
		assertTrue(reteta.existaReteta());	
	}
	@org.junit.Test
	public void testExistaRetetaNU() {
		reteta.faina.setCantitate(0);
		reteta.margarina.setCantitate(0);
		reteta.zahar.setCantitate(0);
		reteta.unt.setCantitate(0);
		reteta.lapte.setCantitate(0);
		assertFalse(reteta.existaReteta());	
	}
	
}
