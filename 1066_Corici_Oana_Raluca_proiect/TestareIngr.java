package Testare;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Clase.Ingredient;
import Clase.RetetaTort; 

public class TestareIngr {

	RetetaTort reteta;
	Ingredient ingr;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		reteta=new RetetaTort();
		ingr=new Ingredient();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testClona() {
		 assertNotSame(ingr,ingr.clone());
	}
	@Test
	public void testCantitateEgla() {
		reteta.getLista().get(2).setCantitate(4);
		  assertEquals(4,reteta.getLista().get(2).getCantitate());		 
	}
	@Test
	public void testClonaIngr() {	
		  assertNotSame(reteta.getLista().get(0),reteta.getLista().get(0).clone());	 
	}

}
